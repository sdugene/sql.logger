/*
 **************************************************
 * @generator      : GeneRest
 * @url            : www.generest.fr
 **************************************************
*/

package com.sdugene.logger;

enum LogLevel {
	DEBUG,
	INFO,
	WARN,
	ERROR,
	FATAL,
	TRACE
}
