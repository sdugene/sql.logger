/*
 **************************************************
 * @generator      : GeneRest
 * @url            : www.generest.fr
 **************************************************
*/

package com.sdugene.logger;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@TypeDef(name = "pgSql_enum", typeClass = PostgreSQLEnumType.class)
@Table(name = "log", schema = "public")
public class Log {

	@Id
	@Column(name = "id")
	private UUID id;

	@Column(name = "level", columnDefinition = " log_level")
	@Enumerated(EnumType.STRING)
	@Type(type = "pgSql_enum")
	private LogLevel level;

	@Column(name = "message")
	private String message;

	@Column(name = "class_name")
	private String className;

	@Column(name = "date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	Log() {

	}

	Log(Log logSource) {
		this.id = logSource.getId();
		this.level = logSource.getLevel();
		this.message = logSource.getMessage();
		this.className = logSource.getClassName();
		this.date = logSource.getDate();
	}

	void setId(UUID id) {
		this.id = id;
	}

	void setLevel(LogLevel level) {
		this.level = level;
	}

	void setMessage(String message) {
		this.message = message;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	private UUID getId() {
		return id;
	}

	private LogLevel getLevel() {
		return level;
	}

	private String getMessage() {
		return message;
	}

	private String getClassName() {
		return className;
	}

	void setClassName(String className) {
		this.className = className;
	}

	public Date getDate() {
		return date;
	}
}
