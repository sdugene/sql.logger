/*
 **************************************************
 * @generator      : GeneRest
 * @url            : www.generest.fr
 **************************************************
*/

package com.sdugene.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LogService {

	private LogRepository logRepository;

	@Autowired
	LogService setLogRepository(LogRepository logRepository) {
		this.logRepository = logRepository;
		return this;
	}

	Log get(UUID id) {
		return logRepository.existsById(id) ? logRepository.getOne(id) : null;
	}

	List<Log> getAll() {
		return logRepository.findAll();
	}

	Log create(Log log) {
		log.setId(UUID.randomUUID());
		return logRepository.save(log);
	}

	Log update(Log log) {
		return logRepository.save(log);
	}

	boolean deleteById(UUID id) {
		if (logRepository.existsById(id)) {
			logRepository.deleteById(id);
			return true;
		} else {
			return false;
		}
	}
}
