/*
 **************************************************
 * @generator      : GeneRest
 * @url            : www.generest.fr
 **************************************************
*/

package com.sdugene.logger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface LogRepository extends JpaRepository<Log, UUID> {
}
