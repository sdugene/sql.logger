package com.sdugene.logger;


import java.util.Date;

public class Logger {

    private LogService logService;

    private Class<?> clazz;

    Logger(Class<?> clazz, LogService logService) {
        this.clazz = clazz;
        this.logService = logService;
    }

    public void debug(String message) {
        this.createLog(LogLevel.DEBUG, message);
    }

    public void debug(String message, String... args) {
        this.createLog(LogLevel.DEBUG, insertArgs(message, args));
    }

    public void debug(String message, Object... args) {
        this.createLog(LogLevel.DEBUG, insertArgs(message, args));
    }

    public void info(String message) {
        this.createLog(LogLevel.INFO, message);
    }

    public void info(String message, String... args) {
        this.createLog(LogLevel.INFO, insertArgs(message, args));
    }

    public void info(String message, Object... args) {
        this.createLog(LogLevel.INFO, insertArgs(message, args));
    }

    public void warn(String message) {
        this.createLog(LogLevel.WARN, message);
    }

    public void warn(String message, String... args) {
        this.createLog(LogLevel.WARN, insertArgs(message, args));
    }

    public void warn(String message, Object... args) {
        this.createLog(LogLevel.WARN, insertArgs(message, args));
    }

    public void error(String message) {
        this.createLog(LogLevel.ERROR, message);
    }

    public void error(String message, String... args) {
        this.createLog(LogLevel.ERROR, insertArgs(message, args));
    }

    public void error(String message, Object... args) {
        this.createLog(LogLevel.ERROR, insertArgs(message, args));
    }

    public void fatal(String message) {
        this.createLog(LogLevel.FATAL, message);
    }

    public void fatal(String message, String... args) {
        this.createLog(LogLevel.FATAL, insertArgs(message, args));
    }

    public void fatal(String message, Object... args) {
        this.createLog(LogLevel.FATAL, insertArgs(message, args));
    }

    public void trace(String message) {
        this.createLog(LogLevel.TRACE, message);
    }

    public void trace(String message, String... args) {
        this.createLog(LogLevel.TRACE, insertArgs(message, args));
    }

    public void trace(String message, Object... args) {
        this.createLog(LogLevel.TRACE, insertArgs(message, args));
    }

    private void createLog(LogLevel level, String message) {
        Log log = new Log();
        log.setLevel(level);
        log.setClassName(clazz.getName());
        log.setDate(new Date());
        log.setMessage(message);
        logService.create(log);
    }

    private String insertArgs(String message, String[] arguments) {
        for(String string : arguments) {
            message = message.replaceFirst("[{}]+", string);
        }
        return message;
    }

    private String insertArgs(String message, Object[] arguments) {
        for(Object object : arguments) {
            message = message.replaceFirst("[{}]+", object.toString());
        }
        return message;
    }
}
