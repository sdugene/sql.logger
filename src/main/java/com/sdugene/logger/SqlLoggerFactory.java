package com.sdugene.logger;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class SqlLoggerFactory {

    private SqlLoggerFactory() {
        throw new IllegalStateException("Utility class");
    }

    public static Logger getLogger(Class<?> clazz) {
        LogService logService = BeanUtil.getBean(LogService.class);
        return new Logger(clazz, logService);
    }
}
